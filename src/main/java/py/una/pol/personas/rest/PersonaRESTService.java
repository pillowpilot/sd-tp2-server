package py.una.pol.personas.rest;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.PersonaService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Path("/personas")
@RequestScoped
public class PersonaRESTService
{
    @Inject
    PersonaService personaService;
    @Inject
    private Logger log;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona> listar()
    {
        return personaService.seleccionar();
    }

    @GET
    @Path("/{cedula:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona obtenerPorId(@PathParam("cedula") long cedula)
    {
        Persona p = personaService.seleccionarPorCedula(cedula);
        if (p == null) {
            log.info("obtenerPorId " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
        return p;
    }

    @GET
    @Path("/cedula")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona obtenerPorIdQuery(@QueryParam("cedula") long cedula)
    {
        Persona p = personaService.seleccionarPorCedula(cedula);
        if (p == null) {
            log.info("obtenerPorId " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
        return p;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Persona p)
    {

        Response.ResponseBuilder builder = null;

        try {
            personaService.crear(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Persona creada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @DELETE
    @Path("/{cedula}")
    public Response borrar(@PathParam("cedula") long cedula)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (personaService.seleccionarPorCedula(cedula) == null) {

                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
            } else {
                personaService.borrar(cedula);
                builder = Response.status(202).entity("Persona borrada exitosamente.");
            }


        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @GET
    @Path("/{cedula}/asignaturas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAsignaturasDePersona(@PathParam("cedula") long cedula)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (personaService.seleccionarPorCedula(cedula) == null) {

                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
            } else {
                List<Asignatura> listaDeAsignaturas = personaService.listarAsignaturasDePersona(cedula);
                builder = Response.status(202).entity(listaDeAsignaturas);
            }

        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}
