package py.una.pol.personas.rest;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;
import py.una.pol.personas.service.PersonaService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService
{
    @Inject
    AsignaturaService asignaturaService;

    @Inject
    PersonaService personaService;

    @Inject
    private Logger log;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar()
    {
        return asignaturaService.seleccionar();
    }

    @GET
    @Path("/id")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerPorId(@QueryParam("id") long id)
    {
        Asignatura asignatura = asignaturaService.seleccionarPorId(id);
        if (asignatura == null) {
            log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + asignatura.getNombre());
        return asignatura;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asignatura p)
    {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.crear(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura creada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Asignatura p)
    {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.actualizar(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura actualizada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response borrar(@PathParam("id") long id)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (asignaturaService.seleccionarPorId(id) == null) {

                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
            } else {
                asignaturaService.borrar(id);
                builder = Response.status(202).entity("Asignatura borrada exitosamente.");
            }


        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @GET
    @Path("/{id}/personas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPersonasDeAsignatura(@PathParam("id") long id)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (asignaturaService.seleccionarPorId(id) == null) {

                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
            } else {
                List<Persona> listaDePersonas = asignaturaService.listarPersonasDeAsignatura(id);
                builder = Response.status(202).entity(listaDePersonas);
            }

        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @PUT
    @Path("/{id}/persona/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarPersonaAAsignatura(@PathParam("id") long id, @PathParam("cedula") long cedula)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (asignaturaService.seleccionarPorId(id) == null) {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
            } else if (personaService.seleccionarPorCedula(cedula) == null) {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
            } else {
                asignaturaService.agregarPersonaAAsignatura(id, cedula);
                builder = Response.status(202).entity("Relacion agregada");
            }

        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @DELETE
    @Path("/{id}/persona/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarPersonaDeAsignatura(@PathParam("id") long id, @PathParam("cedula") long cedula)
    {
        Response.ResponseBuilder builder = null;
        try {

            if (asignaturaService.seleccionarPorId(id) == null) {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
            } else if (personaService.seleccionarPorCedula(cedula) == null) {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
            } else {
                asignaturaService.eliminarPersonaDeAsignatura(id, cedula);
                builder = Response.status(202).entity("Relacion eliminada");
            }

        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}
