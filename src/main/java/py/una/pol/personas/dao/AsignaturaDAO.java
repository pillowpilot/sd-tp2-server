package py.una.pol.personas.dao;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class AsignaturaDAO
{
    @Inject
    private Logger log;

    public List<Asignatura> seleccionar()
    {
        String query = "SELECT id, nombre FROM asignatura ORDER BY nombre";

        List<Asignatura> lista = new ArrayList<>();

        Connection conn = null;
        try {
            conn = Bd.connect();
            ResultSet rs = conn.createStatement().executeQuery(query);

            while (rs.next()) {
                Long id = rs.getLong(1);
                String nombre = rs.getString(2);

                Asignatura p = new Asignatura(id, nombre);

                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    public Asignatura seleccionarPorId(long id)
    {
        String SQL = "SELECT nombre FROM asignatura WHERE id = ? ";

        Asignatura asignatura = null;

        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String nombre = rs.getString(1);

                asignatura = new Asignatura(id, nombre);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return asignatura;

    }

    public long insertar(Asignatura asignatura) throws SQLException
    {

        String SQL = "INSERT INTO asignatura(id, nombre) "
                + "VALUES(?,?)";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, asignatura.getId());
            pstmt.setString(2, asignatura.getNombre());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return id;

    }

    public long actualizar(Asignatura asignatura) throws SQLException
    {

        String SQL = "UPDATE asignatura SET nombre = ? WHERE id = ? ";

        long id = 0;
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, asignatura.getNombre());
            pstmt.setLong(2, asignatura.getId());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la actualizacion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return id;
    }

    public long borrar(long id) throws SQLException
    {

        String SQL = "DELETE FROM asignatura WHERE id = ? ";


        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, id);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    log.severe("Error en la eliminación: " + ex.getMessage());
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la eliminación: " + ex.getMessage());
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
                throw ef;
            }
        }
        return id;
    }

    public List<Persona> listarPersonasDeAsignatura(long id)
    {
        String query = "select cedula, nombre, apellido from persona_asignatura inner join persona on persona_asignatura.persona_cedula = persona.cedula where asignatura_id = ?";

        List<Persona> lista = new ArrayList<>();

        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1, id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Long cedula = rs.getLong(1);
                String nombre = rs.getString(2);
                String apellido = rs.getString(3);

                Persona p = new Persona(cedula, nombre, apellido);

                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    public long agregarPersona(long id, long cedula)
            throws SQLException
    {
        String SQL = "INSERT INTO persona_asignatura(persona_cedula, asignatura_id) "
                + "VALUES(?,?)";

        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, id);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return id;
    }

    public long eliminarPersona(long id, long cedula)
            throws SQLException
    {
        String SQL = "DELETE FROM persona_asignatura WHERE persona_cedula = ? AND asignatura_id = ?";

        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, id);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return id;
    }
}
