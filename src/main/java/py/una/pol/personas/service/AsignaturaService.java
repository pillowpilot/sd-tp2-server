package py.una.pol.personas.service;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class AsignaturaService
{

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public List<Asignatura> seleccionar()
    {

        return dao.seleccionar();
    }

    public Asignatura seleccionarPorId(long id)
    {

        return dao.seleccionarPorId(id);
    }

    public void crear(Asignatura asignatura) throws Exception
    {
        log.info("Creando Asignatura: " + asignatura.getNombre());
        try {
            dao.insertar(asignatura);
        } catch (Exception e) {
            log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage());
            throw e;
        }
        log.info("Asignatura creada con éxito: " + asignatura.getNombre());
    }

    public void actualizar(Asignatura asignatura) throws Exception
    {
        log.info("Actualizando Asignatura: " + asignatura.getNombre());
        try {
            dao.actualizar(asignatura);
        } catch (Exception e) {
            log.severe("ERROR al actualizar asignatura: " + e.getLocalizedMessage());
            throw e;
        }
        log.info("Asignatura actualizada con éxito: " + asignatura.getNombre());
    }

    public long borrar(long id) throws Exception
    {
        return dao.borrar(id);
    }

    public List<Persona> listarPersonasDeAsignatura(long id)
    {
        return dao.listarPersonasDeAsignatura(id);
    }

    public long agregarPersonaAAsignatura(long id, long cedula)
            throws SQLException
    {
        return dao.agregarPersona(id, cedula);
    }

    public long eliminarPersonaDeAsignatura(long id, long cedula)
            throws SQLException
    {
        return dao.eliminarPersona(id, cedula);
    }
}
